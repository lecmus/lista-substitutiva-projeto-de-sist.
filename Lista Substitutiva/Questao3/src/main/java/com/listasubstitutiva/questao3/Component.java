
package com.listasubstitutiva.questao3;

/**
 *
 * @author lucas
 */
public abstract class Component {
    
    String nome;
    int quantidade;
    double precoCusto;
    
    public void adicionaParte(ParteComponent parte) throws Exception{
        throw new Exception("O arquivo não pode ser adicionado");
    }
    
    public void removeParte(String nome) throws Exception{
        throw new Exception("O arquivo não pode ser removido");
    }
    
    public String getNome() {
        return nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public double getPrecoCusto() {
        return precoCusto;
    }
}
