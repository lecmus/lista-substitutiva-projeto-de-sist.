
package com.listasubstitutiva.questao3;

/**
 *
 * @author lucas
 */
public class Principal {
    public static void main(String[] args) {
        
        IBuilderComputador builderComputador = new BuilderComputadorPadrao();
        
        DiretorComputador diretor = new DiretorComputador();
        
        Component computador = diretor.construir(builderComputador);
              
        System.out.println(computador);
    }
}
