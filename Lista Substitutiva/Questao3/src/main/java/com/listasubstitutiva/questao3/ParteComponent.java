
package com.listasubstitutiva.questao3;

/**
 *
 * @author lucas
 */
public class ParteComponent extends Component{
    
    public ParteComponent(String nome, int quantidade, double precoCusto) {
        this.nome = nome;
        this.quantidade = quantidade;
        this.precoCusto = precoCusto;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public int getQuantidade() {
        return quantidade;
    }

    @Override
    public double getPrecoCusto() {
        return precoCusto;
    }

    @Override
    public String toString() {
        return "Peça: " + quantidade + "x " + nome + " --- R$" + precoCusto + " unidade\n";
    }  
}
