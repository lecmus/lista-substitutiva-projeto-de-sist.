
package com.listasubstitutiva.questao3;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lucas
 */
public class BuilderComputadorPadrao implements IBuilderComputador{
    
    @Override
    public void addPartes() {
        try {
            computador.adicionaParte(new ParteComponent("Placa mãe", 1, 1100));
            computador.adicionaParte(new ParteComponent("Placa de video", 1, 1500));
            computador.adicionaParte(new ParteComponent("Memória RAM 8G", 2, 220));
            computador.adicionaParte(new ParteComponent("Hard Disk 1TB", 1, 150));
        } catch (Exception ex) {
            Logger.getLogger(BuilderComputadorPadrao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Component getComputador() {
        return computador;
    }
}
