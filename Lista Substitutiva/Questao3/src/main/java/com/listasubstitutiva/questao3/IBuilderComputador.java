
package com.listasubstitutiva.questao3;

/**
 *
 * @author lucas
 */
public interface IBuilderComputador {
    
    final Component computador = new Computador();
    
    public void addPartes();
    public Component getComputador();
}
