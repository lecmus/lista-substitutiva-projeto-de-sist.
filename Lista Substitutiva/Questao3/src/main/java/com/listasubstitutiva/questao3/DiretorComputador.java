
package com.listasubstitutiva.questao3;

/**
 *
 * @author lucas
 */
public class DiretorComputador {
    
    public Component construir(IBuilderComputador builder){
        
        builder.addPartes();
        return builder.getComputador();
    }
}
