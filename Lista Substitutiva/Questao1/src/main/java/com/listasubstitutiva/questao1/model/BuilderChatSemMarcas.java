
package com.listasubstitutiva.questao1.model;

import com.listasubstitutiva.questao1.removerMarcas.ProcessadoraRemoverMarca;

/**
 *
 * @author lucas
 */
public class BuilderChatSemMarcas implements IBuilderChat{
    
    @Override
    public void setTipoChat() {
        mediator.setProcessadora(new ProcessadoraRemoverMarca());
    }

    @Override
    public IMediatorChat getProduto() {
        return mediator;
    }
}
