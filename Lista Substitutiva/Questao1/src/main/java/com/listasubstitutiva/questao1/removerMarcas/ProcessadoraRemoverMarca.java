
package com.listasubstitutiva.questao1.removerMarcas;

import com.listasubstitutiva.questao1.model.IProcessadoraRemocao;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ProcessadoraRemoverMarca implements IProcessadoraRemocao{
    
    private final ArrayList<RemovedoraMarca> removedores = new ArrayList<>();

    @Override
    public void addRemovedor() {

        removedores.add(new RemovedoraMarca("IBM"));
        removedores.add(new RemovedoraMarca("Apple"));
        removedores.add(new RemovedoraMarca("Microsoft"));
        removedores.add(new RemovedoraMarca("Lenovo"));
        removedores.add(new RemovedoraMarca("HP"));
        removedores.add(new RemovedoraMarca("Sansung"));
        removedores.add(new RemovedoraMarca("Nokia"));
        
    }

    @Override
    public String processaRemovedoras(String texto) {
        for (RemovedoraMarca removedor : removedores) {

            texto = removedor.removeMarca(texto);

        }
        return texto;
    }
}
