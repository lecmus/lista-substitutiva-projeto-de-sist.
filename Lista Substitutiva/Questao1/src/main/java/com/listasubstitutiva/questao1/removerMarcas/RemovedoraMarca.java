
package com.listasubstitutiva.questao1.removerMarcas;

/**
 *
 * @author lucas
 */
public class RemovedoraMarca {
    
     private String nomeMarca;

    public RemovedoraMarca(String nomeMarcaARemover) {
        this.nomeMarca = nomeMarcaARemover;
    }

    public String removeMarca(String texto) {
        return ClasseString.getInstance().substitui(texto, nomeMarca, "*");
    }
}
