
package com.listasubstitutiva.questao1.model;

import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class SalaChat implements IMediatorChat{
    
    private ArrayList<Participante> participants;

    public SalaChat() {
        this.participants = new ArrayList<>();
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Participante criarParticipante(IMediatorChat mediador, String nome) {
        Participante participante = new ParticipanteChat(mediador, nome);
        
        return participante;
    }

    public ArrayList<Participante> getParticipants() {
        return participants;
    }

    @Override
    public void addParticipante(IMediatorChat mediador, String nome) {
        participants.add(this.criarParticipante(mediador, nome));
        System.out.println(nome + " entrou.");
    }

    @Override
    public void setProcessadora(IProcessadoraRemocao processadora) {
        
    }
}
