
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public interface IBuilderChat {
    
    final IMediatorChat mediator = new ProxySalaChat();
    
    public void setTipoChat();
    
    public IMediatorChat getProduto();
}
