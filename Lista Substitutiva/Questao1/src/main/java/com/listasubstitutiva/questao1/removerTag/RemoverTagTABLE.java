/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listasubstitutiva.questao1.removerTag;

/**
 *
 * @author lucas
 */
public class RemoverTagTABLE implements IRemoverTag{
    
    @Override
    public String removeMarca(String texto) {
        return ClasseString.getInstance().substitui(texto, "<table");
    }
}
