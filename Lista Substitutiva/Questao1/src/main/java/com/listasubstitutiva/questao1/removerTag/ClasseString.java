
package com.listasubstitutiva.questao1.removerTag;

/**
 *
 * @author lucas
 */
public class ClasseString {
    
    private static ClasseString instance;

    public static ClasseString getInstance() {
        if (instance == null) {
            instance = new ClasseString();
        }
        return instance;
    }

    public String substitui(String texto, String palavra) {
        if (texto.contains(palavra)){
            return "Mensagem removida por conter conteúdo não autorizado";
        }else{
            return texto;
        }
    }
}
