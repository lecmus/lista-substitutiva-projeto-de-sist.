
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public class ParticipanteChat extends Participante{
    
    public ParticipanteChat(IMediatorChat chatMediador, String participantName) {
        super.setMediator(mediator);
        super.setName(participantName);
    }
    
    @Override
    public String getName(){
        return super.getName();
    }
    
    @Override
    public void enviar(String message){
    
    }
    
    @Override
    public void receber(String message, Participante participante){
    
    }
}
