
package com.listasubstitutiva.questao1.removerTag;

/**
 *
 * @author lucas
 */
public interface IRemoverTag {
    
    public String removeMarca(String texto);
}
