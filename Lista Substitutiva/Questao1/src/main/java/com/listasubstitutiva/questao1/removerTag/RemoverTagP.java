
package com.listasubstitutiva.questao1.removerTag;

/**
 *
 * @author lucas
 */
public class RemoverTagP implements IRemoverTag{
    
    @Override
    public String removeMarca(String texto) {
        return ClasseString.getInstance().substitui(texto, "<p");
    }
}
