
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public class Principal {
    public static void main(String[] args) {
        
        IBuilderChat builderChat = new BuilderChatSemTag();
        IBuilderChat builderChat2 = new BuilderChatSemMarcas();
        
        DiretorChat diretor = new DiretorChat();
        
        IMediatorChat chatSemTAG = diretor.construir(builderChat);
        
        IMediatorChat chatSemMarcas = diretor.construir(builderChat2);
        
        System.out.println("=== Chat sem TAGS ===");
        
        chatSemTAG.addParticipante(chatSemTAG, "Fulano");
        chatSemTAG.addParticipante(chatSemTAG, "Sicrano");
        chatSemTAG.addParticipante(chatSemTAG, "Beltrano");
        
        System.out.println("");
        System.out.println("=== Chat sem marcas ===");
        
        chatSemMarcas.addParticipante(chatSemMarcas, "Fulano");
        chatSemMarcas.addParticipante(chatSemMarcas, "Sicrano");
        chatSemMarcas.addParticipante(chatSemMarcas, "Beltrano");
        
    }
}

