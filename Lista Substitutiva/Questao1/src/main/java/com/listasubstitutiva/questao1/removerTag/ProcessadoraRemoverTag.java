
package com.listasubstitutiva.questao1.removerTag;

import com.listasubstitutiva.questao1.model.IProcessadoraRemocao;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ProcessadoraRemoverTag implements IProcessadoraRemocao{
    
    private final ArrayList<IRemoverTag> removedores = new ArrayList<>();

    @Override
    public void addRemovedor() {
        removedores.add(new RemoverTagIMG());
        removedores.add(new RemoverTagTABLE());
        removedores.add(new RemoverTagP());
        removedores.add(new RemoverTagAHREF());
    }

    @Override
    public String processaRemovedoras(String texto) {
        for (IRemoverTag removedor : removedores) {

            texto = removedor.removeMarca(texto);

        }
        return texto;
    }
}
