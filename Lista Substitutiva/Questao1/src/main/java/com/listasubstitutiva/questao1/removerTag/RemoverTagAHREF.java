
package com.listasubstitutiva.questao1.removerTag;

/**
 *
 * @author lucas
 */
public class RemoverTagAHREF implements IRemoverTag{
    
    @Override
    public String removeMarca(String texto) {
        return ClasseString.getInstance().substitui(texto, "<a href");
    }
}
