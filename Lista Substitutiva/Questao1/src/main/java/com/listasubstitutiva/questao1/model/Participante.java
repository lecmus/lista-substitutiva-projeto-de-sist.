
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public abstract class Participante {
    
    private String name;
    protected IMediatorChat mediator;
    
    public String getName(){
        return name;
    }
    
    public abstract void enviar(String message);
    
    public abstract void receber(String message, Participante participante);

    public void setName(String name) {
        this.name = name;
    }

    public void setMediator(IMediatorChat mediator) {
        this.mediator = mediator;
    }
}
