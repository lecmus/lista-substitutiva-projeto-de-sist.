
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public interface IMediatorChat {
    
    public Participante criarParticipante(IMediatorChat mediador, String nome);
    public void enviar(Participante participante, String mensagem);
    public void addParticipante(IMediatorChat mediador, String nome);
    public void setProcessadora(IProcessadoraRemocao processadora);
}
