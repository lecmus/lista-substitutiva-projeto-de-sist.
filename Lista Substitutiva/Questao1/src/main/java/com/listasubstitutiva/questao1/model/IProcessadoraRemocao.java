
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public interface IProcessadoraRemocao {
    
    public void addRemovedor();
    public String processaRemovedoras(String texto);
}
