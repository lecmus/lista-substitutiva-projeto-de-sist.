
package com.listasubstitutiva.questao1.removerTag;

/**
 *
 * @author lucas
 */
public class RemoverTagIMG implements IRemoverTag {
    
    @Override
    public String removeMarca(String texto) {
        return ClasseString.getInstance().substitui(texto, "<img");
    }
}
