
package com.listasubstitutiva.questao1.model;

import com.listasubstitutiva.questao1.removerTag.ProcessadoraRemoverTag;

/**
 *
 * @author lucas
 */
public class BuilderChatSemTag implements IBuilderChat{
    
    @Override
    public void setTipoChat() {
        mediator.setProcessadora(new ProcessadoraRemoverTag());
    }

    @Override
    public IMediatorChat getProduto() {
        return mediator;
    }
}
