
package com.listasubstitutiva.questao1.removerMarcas;

/**
 *
 * @author lucas
 */
public class ClasseString {
    
    private static ClasseString instance;

    public static ClasseString getInstance() {
        if (instance == null) {
            instance = new ClasseString();
        }
        return instance;
    }

    public String substitui(String texto, String palavra, CharSequence simbolo) {
        return texto.replaceAll(palavra, new String(new char[palavra.length()]).replace("\0", simbolo));
    }
}
