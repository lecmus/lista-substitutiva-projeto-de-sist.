
package com.listasubstitutiva.questao1.model;

/**
 *
 * @author lucas
 */
public class ProxySalaChat implements IMediatorChat{
    
    public SalaChat salaChat;
    public IProcessadoraRemocao processadora;
    
    public ProxySalaChat() {
        this.salaChat = new SalaChat();
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        processadora.addRemovedor();
        salaChat.enviar(participante, processadora.processaRemovedoras(mensagem));
    }

    @Override
    public Participante criarParticipante(IMediatorChat mediador, String nome) {
        return salaChat.criarParticipante(mediador, nome);
    }
    
    @Override
    public void addParticipante(IMediatorChat mediador, String nome){
        salaChat.addParticipante(mediador, nome);
    }
    
    @Override
    public void setProcessadora(IProcessadoraRemocao processadora){
        this.processadora = processadora;
    }
}
