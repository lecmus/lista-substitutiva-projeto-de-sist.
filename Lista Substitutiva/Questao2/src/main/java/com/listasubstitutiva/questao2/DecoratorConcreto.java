
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public class DecoratorConcreto extends DecoratorSanduiche{
    
    public DecoratorConcreto(ISanduiche sanduiche) {
        super(sanduiche);
    }
    
    @Override
    public void addIngrediente(Ingrediente ingrediente){
        sanduiche.addIngrediente(ingrediente);
    }
}
