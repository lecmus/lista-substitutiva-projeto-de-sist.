
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public class BuilderMistoQuente implements IBuilderSanduiche{
    
    @Override
    public void addPao() {
        sanduiche.addIngrediente(new Ingrediente("Pão de forma", 2));
    }

    @Override
    public void addCarne() {
        sanduiche.addIngrediente(new Ingrediente("Presunto", 1));
    }
    
    @Override
    public void addComplementos() {
        sanduiche.addIngrediente(new Ingrediente("Queijo", 1));
    }

    @Override
    public Sanduiche getProduto() {
        return sanduiche;
    }
}
