
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public class Principal {
    
    public static void main(String[] args) {
        /*EXECUTAR UM SANDUICHE APÓS O OUTRO PARA MELHOR VISUALIZAÇÃO*/
        
        DiretorSanduiche diretor = new DiretorSanduiche();
        
        //BEIRUTE
//  /*
        IBuilderSanduiche builderBeirute = new BuilderBeirute();
        ISanduiche beirute = diretor.construir(builderBeirute);
        //acrescentar ingrediente ao beirute
        Ingrediente ingrediente = new Ingrediente("Alface", 1);
        DecoratorSanduiche decorator = new DecoratorConcreto(beirute);
        decorator.addIngrediente(ingrediente);
        
        System.out.println("=== Beirute ===");
        System.out.println(beirute);
        System.out.println("-----");
//  */
        
        //MISTO
  /* 
        IBuilderSanduiche builderMistoQuente = new BuilderMistoQuente();
        ISanduiche mistoQuente = diretor.construir(builderMistoQuente);
        
        System.out.println("=== Misto Quente ===");
        System.out.println(mistoQuente);
        System.out.println("-----");
  */
  
    }
}
