
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public class BuilderBeirute implements IBuilderSanduiche{
    
    @Override
    public void addPao() {
        sanduiche.addIngrediente(new Ingrediente("Pão sírio", 1));
    }

    @Override
    public void addCarne() {
        sanduiche.addIngrediente(new Ingrediente("Rosbife", 1));
    }

    @Override
    public void addComplementos() {
        sanduiche.addIngrediente(new Ingrediente("Queijo", 2));
        sanduiche.addIngrediente(new Ingrediente("Alface", 1));
        sanduiche.addIngrediente(new Ingrediente("Tomate", 1));
        sanduiche.addIngrediente(new Ingrediente("Ovo frito", 2));
    }

    @Override
    public Sanduiche getProduto() {
        return sanduiche;
    }
}
