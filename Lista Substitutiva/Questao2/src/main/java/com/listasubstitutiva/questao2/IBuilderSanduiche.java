
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public interface IBuilderSanduiche {
    
    final Sanduiche sanduiche = new Sanduiche();
    
    public void addPao();
    public void addCarne();
    public void addComplementos();
    
    public Sanduiche getProduto();
}
