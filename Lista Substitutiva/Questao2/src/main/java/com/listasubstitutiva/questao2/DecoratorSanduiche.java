
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public abstract class DecoratorSanduiche extends ISanduiche{
    
    ISanduiche sanduiche;

    public DecoratorSanduiche(ISanduiche sanduiche) {
        this.sanduiche = sanduiche;
    }
    
    @Override
    public void addIngrediente(Ingrediente ingrediente){}

    public void setSanduiche(ISanduiche sanduiche) {
        this.sanduiche = sanduiche;
    }
}
