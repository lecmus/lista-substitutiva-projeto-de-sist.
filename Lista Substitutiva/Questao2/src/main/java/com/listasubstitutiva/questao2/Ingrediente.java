
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public class Ingrediente {
    
    private final String nome;
    private final int quantidade;

    public Ingrediente(String nome, int quantidade) {
        this.nome = nome;
        this.quantidade = quantidade;
    }

    public String getNome() {
        return nome;
    }

    public int getQuantidade() {
        return quantidade;
    }
}
