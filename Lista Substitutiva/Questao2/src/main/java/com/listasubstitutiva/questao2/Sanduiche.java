
package com.listasubstitutiva.questao2;

import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class Sanduiche extends ISanduiche {
    
    ArrayList<Ingrediente> ingredientes;

    public Sanduiche() {
        this.ingredientes = new ArrayList<>();
    }
   
    public ArrayList<Ingrediente> getIngredientes() {
        return ingredientes;
    }
    
    @Override
    public void addIngrediente(Ingrediente ingrediente){
        ingredientes.add(ingrediente);
    }

    @Override
    public String toString() {
        String descricaoSanduiche = "Ingredientes: \n";
        
        for(Ingrediente ingrediente: ingredientes){
            descricaoSanduiche += ingrediente.getNome() + " ---> Quantidade: " + ingrediente.getQuantidade() + "\n"; 
        }
        return descricaoSanduiche;
    }
}
