
package com.listasubstitutiva.questao2;

/**
 *
 * @author lucas
 */
public class DiretorSanduiche {
    
    public Sanduiche construir(IBuilderSanduiche builder){
        builder.addPao();
        builder.addCarne();
        builder.addComplementos();
        
        return builder.getProduto();
    }
}
